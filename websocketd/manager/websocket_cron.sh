#!/usr/bin/env bash

if crontab -l | grep -q 'websocket_running'
then
   echo 'Cron job exists'
else
  (crontab -l 2>/dev/null; echo "*/5 * * * * /home/pi/argon-scripts/websocketd/manager/keep_websocket_running.sh") | crontab -

  echo 'Added cron job'
fi

sh /home/pi/argon-scripts/websocketd/manager/keep_websocket_running.sh