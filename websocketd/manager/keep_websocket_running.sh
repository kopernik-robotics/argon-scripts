#!/bin/bash
#make-run.sh
#make sure a process is always running.

process="websocketd"
makerun="/usr/bin/websocketd --devconsole --port=8080 bash /home/pi/argon-scripts/websocketd/manager/wsmanager.sh 2>&1"

if pgrep -f "websocketd --port=8080" > /dev/null
then
    echo "Web socket is already running"
    exit
else
    echo "Starting web socket"
    $makerun &
fi

exit