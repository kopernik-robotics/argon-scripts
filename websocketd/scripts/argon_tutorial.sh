#!/bin/bash

if [ $# -gt 0 ]; then
    sudo cp -r /home/pi/argon-scripts/docs/tutorials/$1 /var/www/html/workspace/$2
    sudo chown www-data:www-data /var/www/html/workspace/$2 -R
    echo 'refreshFileManager'
else
    echo "Missing arguments"
fi
exit