#!/bin/bash

if [ $# -gt 0 ]; then
    sudo openocd -f /home/pi/argon-scripts/openocd/raspberry3_f446re.cfg -c init -c "reset halt" -c "flash write_image erase "$1" 0x08000000" -c shutdown
else
    echo "Missing arguments"
fi
exit