#!/bin/bash
#This script just echos a string or hello

if [ $# -gt 0 ]; then
    #Cd to mbed-os path
    cd /home/pi/argon-scripts/mbed/

    #Removing existing build
    rm -rf $1/BUILD

    #Compiling new build
    mbed compile -t GCC_ARM -m NUCLEO_F446RE --source=$1 --source=mbed-os-build --build=$1/BUILD --profile mbed-os/tools/profiles/debug.json
    sudo chown www-data:www-data $1/BUILD -R
else
    echo "Missing arguments"
fi

echo 'refreshFileManager'
exit