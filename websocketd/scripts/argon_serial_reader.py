#!/usr/bin/env python
import sys
import serial

try:
        try:
                ser = serial.Serial('/dev/ttyACM0',9600)
        except serial.serialutil.SerialException:
                print "No device available!"
                sys.exit()
        #Print Variables
        while True:
                try:
                        read = ser.readline()
                except serial.serialutil.SerialException:
                        print "Device disconnected!"
                        sys.exit(0)
                print str(read)
                sys.stdout.flush()
except KeyboardInterrupt:
    sys.exit(0)
