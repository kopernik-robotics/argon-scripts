# Argon 

### Installing Argon Web



```
sudo raspi-config #Enable ssh and connect wifi
sudo su
wget -q https://bitbucket.org/kopernik-robotics/argon-scripts/raw/master/installer/raspbian.sh -O /tmp/argon && bash /tmp/argon
```

We should append url.rewrite-if-not-file = ("^" => "/index.php") to lighthttpd 
conf for mod rewrite
 
## Authors

* **Kerem Can Kaya**
* **Pamir Mundt**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Thanks for great projects!

* https://github.com/billz/raspap-webgui
* https://github.com/Codiad/Codiad

sudo gpasswd -a pi www-data
chmod 775 /var/www/workspace/



