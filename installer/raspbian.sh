#!/usr/bin/env bash
#Lightttpd user and path
server_user="www-data"
webroot_dir="/var/www/html"

# Outputs installation line
function install_log() {
    echo -e "\033[1;32mArgon Install: $*\033[m"
}

# Outputs error line
function install_error() {
    echo -e "\033[1;37;41mArgon Install Error: $*\033[m"
    exit 1
}

# Runs a system software update to make sure we're using all fresh packages
function update_system_packages() {
    install_log "Updating sources"
    sudo apt-get update || install_error "Unable to update package list"
}

# Installs additional dependencies using system package manager
function install_dependencies() {
    install_log "Installing required packages"
    sudo apt-get -y install lighttpd php7.0-cgi git hostapd dnsmasq php7.0-zip php7.0-xml php7.0-mbstring || install_error "Unable to install dependencies"
}

# Enables PHP for lighttpd and restarts service for settings to take effect
function enable_php_lighttpd() {
    install_log "Enabling PHP for lighttpd"

    sudo lighttpd-enable-mod fastcgi-php
    sudo service lighttpd force-reload
    sudo /etc/init.d/lighttpd restart || install_error "Unable to restart lighttpd"
}

# Sets files ownership in web root directory
function change_file_ownership() {
    if [ ! -d "$webroot_dir" ]; then
        install_error "Web root directory doesn't exist"
    fi

    install_log "Changing file ownership in web root directory"
    sudo chown -R $server_user:$server_user "$webroot_dir" || install_error "Unable to change file ownership for '$webroot_dir'"
}

# Add a single entry to the sudoers file
function sudo_add() {
    sudo bash -c "echo \"www-data ALL=(ALL) NOPASSWD:$1\" | (EDITOR=\"tee -a\" visudo)" \
       || install_error "Unable to patch /etc/sudoers"
}

# Adds www-data user to the sudoers file with restrictions on what the user can execute
function patch_system_files() {
    # Set commands array
    cmds=(
        '/usr/local/bin/openocd'
        '/sbin/ifdown wlan0'
        '/sbin/ifup wlan0'
        '/bin/cat /etc/wpa_supplicant/wpa_supplicant.conf'
        '/bin/cp /tmp/wifidata /etc/wpa_supplicant/wpa_supplicant.conf'
        '/sbin/wpa_cli scan_results'
        '/sbin/wpa_cli scan'
        '/sbin/wpa_cli reconfigure'
        '/bin/cp /tmp/hostapddata /etc/hostapd/hostapd.conf'
        '/etc/init.d/hostapd start'
        '/etc/init.d/hostapd stop'
        '/etc/init.d/dnsmasq start'
        '/etc/init.d/dnsmasq stop'
        '/bin/cp /tmp/dhcpddata /etc/dnsmasq.conf'
        '/sbin/shutdown -h now'
        '/sbin/reboot'
        '/sbin/ip link set wlan0 down'
        '/sbin/ip link set wlan0 up'
        '/sbin/ip -s a f label wlan0'
        '/bin/cp /etc/raspap/networking/dhcpcd.conf /etc/dhcpcd.conf'
        '/etc/raspap/hostapd/enablelog.sh'
        '/etc/raspap/hostapd/disablelog.sh'
    )

    # Check if sudoers needs patchin
    if [ $(sudo grep -c www-data /etc/sudoers) -ne 15 ]; then
        # Sudoers file has incorrect number of commands. Wiping them out.
        install_log "Cleaning sudoers file"
        sudo sed -i '/www-data/d' /etc/sudoers
        install_log "Patching system sudoers file"
        # patch /etc/sudoers file
        for cmd in "${cmds[@]}"; do
            sudo_add $cmd
        done
    else
        install_log "Sudoers file already patched"
    fi
}

function download_scripts(){
    install_log "Downloading argon scripts"

    argon_scripts_path=/home/pi/argon-scripts
    if [ -d "$argon_scripts_path" ]; then
        sudo mv $argon_scripts_path "$argon_scripts_path.`date +%F-%R`" || install_error "Unable to remove old webroot directory"
    fi

    git clone https://bitbucket.org/kopernik-robotics/argon-scripts /home/pi/argon-scripts || install_error "Unable to download files from github"

}

function download_editor(){
    install_log "Downloading argon editor"

    if [ -d "$webroot_dir" ]; then
        sudo mv $webroot_dir "$webroot_dir.`date +%F-%R`" || install_error "Unable to remove old webroot directory"
    fi

    git clone https://bitbucket.org/kopernik-robotics/argon-editor /tmp/argon-editor || install_error "Unable to download files from github"
    sudo mv /tmp/argon-editor $webroot_dir || install_error "Unable to move raspap-webgui to web root"
    #Codiad requirements
    sudo mkdir /var/www/html/workspace
    sudo mkdir /var/www/html/data
}

function install_websocketd(){
    install_log "Installing websocketd"

    cd /home/pi
    sudo apt-get -y install unzip
    #This file also exists in argon-scripts/websocketd/release as a backup
    wget http://github.com/joewalnes/websocketd/releases/download/v0.2.11/websocketd-0.2.11-linux_arm.zip
    unzip websocketd-0.2.11-linux_arm.zip
    sudo mv websocketd /usr/bin/

}

function install_mbed(){
   # Do not forget to clone mbed source from git
   install_log "Installing Mbed"
   install_log "Do not forget to clone mbed source from git"

   sudo apt-get -y install python-pip mercurial gcc-arm-none-eabi
   sudo pip install mbed-cli

   mbed_gcc_arm_dir="${which arm-none-eabi-gcc}"
   mbed config -G MBED_GCC_ARM_PATH "${mbed_gcc_arm_dir}"

   cd /home/pi/argon-scripts/mbed/
   mbed compile -t GCC_ARM -m NUCLEO_F446RE --library --no-archive --source=mbed-os --build=mbed-os-build

}

function install_openocd(){
   install_log "Installing Openocd"

   cd ~/
   sudo apt-get -y install git autoconf libtool make pkg-config libusb-1.0-0 libusb-1.0-0-dev
   git clone git://git.code.sf.net/p/openocd/code openocd-code
   cd openocd-code
   ./bootstrap
   ./configure --enable-sysfsgpio --enable-bcm2835gpio
   make
   sudo make install

}

# Set up default configuration
function default_configuration() {
    install_log "Default configuration"

    sudo sh /home/pi/argon-scripts/websocketd/manager/websocket_cron.sh
    sudo sh /home/pi/argon-scripts/websocketd/manager/keep_websocket_running.sh

    line="sh /home/pi/argon-scripts/websocketd/manager/keep_websocket_running.sh"

    if grep "$line" /etc/rc.local > /dev/null; then
        echo "$line: Line already added"
    else
        sed -i '/exit 0/d' /etc/rc.local
        echo $line >> /etc/rc.local
        echo $'\nexit 0' >> /etc/rc.local
        echo "Adding line $line"
    fi

}

function set_locale(){
    install_log "Setting keyboard layout"

    L='tr' && sudo sed -i 's/XKBLAYOUT=\"\w*"/XKBLAYOUT=\"'$L'\"/g' /etc/default/keyboard
    install_log "Setting timezone"
    echo "Europe/Istanbul" > /etc/timezone
    dpkg-reconfigure -f noninteractive tzdata
}

function install_complete() {
    echo -n "The system needs to be rebooted as a final step. Reboot now? [y/N]: "
    read answer
    if [[ $answer != "y" ]]; then
        echo "Installation aborted."
        exit 0
    fi
    sudo shutdown -r now || install_error "Unable to execute shutdown"
}

function install_raspap() {
    update_system_packages
    install_dependencies
    enable_php_lighttpd
    change_file_ownership
    patch_system_files
    download_scripts
    download_editor
    change_file_ownership
    install_websocketd
    install_mbed
    install_openocd
    default_configuration
    set_locale
    install_complete
}

install_raspap
