/*
	Hello World Serial Example
	Prints "Hello World" over serial every second
*/

#include "mbed.h"
#include "Argon.h"

// create a serial at 9600 bits per second:
Serial serial(serialTX, serialRX, 9600);

int main() {
    while(1) {
    	//Print Hello World
		serial.printf("Hello world");
		//wait 1 second
		wait(1);
    }
}
