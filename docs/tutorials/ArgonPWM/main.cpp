/*
  Argon PWM(pulse width modulation)
    Drive Argon Base/Arm's joints with PWM and read the current PWM
    values over serial

    PWM Range (12-bit): -4096 < PWM < 4096

    PWM Set funtions:
        void myBase.joint1.setPWM(int16_t pwmValue)
        void myBase.setPWM(uint8_t jointNumber, int16_t pwmValue)
    
    WM Get functions - return PWM as in16_t
        int16_t myBase.getPWM.joint1()
        int16_t myBase.getPWM(uint8_t jointNumber)

 */

#include "mbed.h"
#include "Argon.h"

// Create an Argon Base
ArgonBase myBase;

// create a serial at 9600 bits per second:
Serial serial(serialTX, serialRX, 9600);

int main() {

    // Set joint 1 PWM (front left wheel)
    //  PWM range (-)
    myBase.joint1.setPWM(512);

    // Set joint 2 PWM (front right wheel)
    myBase.joint2.setPWM(-512);

    // Set joint 3 PWM (rear left wheel)
    myBase.joint3.setPWM(512);

    // Set joint 4 PWM (rear right wheel)
    myBase.joint4.setPWM(-512);

    while(1) {

        // Set joint 1 PWM (front left wheel)
        // PWM range (-)
        int joint_pwm1 = myBase.joint1.getPWM();

        // Set joint 2 PWM (front right wheel)
        int joint_pwm2 = myBase.joint2.getPWM();

        // Set joint 3 PWM (rear left wheel)
        int joint_pwm3 = myBase.joint3.getPWM();

        // Set joint 4 PWM (rear right wheel)
        int joint_pwm4 = myBase.joint4.getPWM();

        // Print joint PWM values over serial
        serial.printf("%d %d %d %d \r\n", joint_pwm1, joint_pwm2, joint_pwm3, joint_pwm4);

        // Wait 1 second
        wait(1);
    }
}
