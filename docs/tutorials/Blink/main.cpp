/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 */

#include "mbed.h"
#include "Argon.h"

// Set LED1 (PA_5) as digital output
DigitalOut myled(LED1);

int main() {
    while(1) {
    	// Set LED1 High
        myled = 1;
        // Wait 1 second
        wait(1);
        // Set LED1 Low
        myled = 0;
        // Wait 1 second
        wait(1);
    }
}
