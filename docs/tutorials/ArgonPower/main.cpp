/*
  Argon Power
    Drive Argon Base/Arm's joints with power and read the current power
    values over serial

    Power Range : -100% < PWM < 100%

    Power get funtions:
        void myBase.joint1.setPower(float powerValue)
        void myBase.setPower(uint8_t jointNumber, float powerValue)

    Power get functions - returns power as float:
        float myBase.joint1.getPower()
        float myBase.getPower(uint8_t jointNumber)
 */

#include "mbed.h"
#include "Argon.h"

// Create an Argon Base
ArgonBase myBase;

// create a serial at 9600 bits per second:
Serial serial(serialTX, serialRX, 9600);

int main() {

    // Set joint 1 power (front left wheel)
    //  PWM range (-)
    myBase.joint1.setPower(25.0f);

    // Set joint 2 power (front right wheel)
    myBase.joint2.setPower(-25.0f);

    // Set joint 3 power (rear left wheel)
    myBase.joint3.setPower(25.0f);

    // Set joint 4 power (rear right wheel)
    myBase.joint4.setPower(-25.0f);

    while(1) {

        // Set joint 1 PWM (front left wheel)
        // PWM range (-)
        int joint_power1 = myBase.joint1.getPower();

        // Set joint 2 PWM (front right wheel)
        int joint_power2 = myBase.joint2.getPower();

        // Set joint 3 PWM (rear left wheel)
        int joint_power3 = myBase.joint3.getPower();

        // Set joint 4 PWM (rear right wheel)
        int joint_power4 = myBase.joint4.getPower();

        // Print joint PWM values over serial
        serial.printf("%d %d %d %d \r\n", joint_power1, joint_power2, joint_power3, joint_power4);

        // Wait 1 second
        wait(1);
    }
}
